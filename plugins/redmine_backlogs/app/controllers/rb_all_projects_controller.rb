#include RbCommonHelper

class RbAllProjectsController < ApplicationController
  unloadable

  before_filter :authorize_global,:except => [:my_tasks_and_stories,:tooltip,:save_filters]#, :only => :statistics
  #before_filter :load_project, :authorize, :only => :my_tasks_and_stories

  def rb_jquery_plugins
    @rb_jquery_plugins
  end
  def rb_jquery_plugins=(html)
    @rb_jquery_plugins = html
  end

  def statistics
    backlogs_projects = RbCommonHelper.find_backlogs_enabled_active_projects
    @projects = []
    backlogs_projects.each{|p|
      @projects << p if p.visible? && p.rb_project_settings.show_in_scrum_stats
    }
    @projects.sort! {|a, b| a.scrum_statistics.score <=> b.scrum_statistics.score}
  end

  def my_tasks_and_stories
    $projects_all = Array.new
    backlogs_projects = RbCommonHelper.find_backlogs_enabled_active_projects
    @projects = []

    backlogs_projects.each{|p|
      @projects << p if p.visible?
    }

    @projects.sort! {|a, b| a.scrum_statistics.score <=> b.scrum_statistics.score}
    $projects_all = @projects

    @stories = []
    #@sprint =RbSprint.find(1)
    @sprints = []
    #@project = @projects.first
    @projects.each do |project|      
      RbSprint.open_sprints(project).each do |sprint|
        @sprints << sprint
         sprint.fixed_issues.each do |fix_ist|
          if fix_ist.parent_id.nil?
            if fix_ist.assigned_to == User.current || fix_ist.author == User.current
              @stories << fix_ist
            else
              array = RbStory.where(:parent_id => fix_ist.id, :assigned_to_id => User.current.id)
              array2 = RbStory.where(:parent_id => fix_ist.id, :author_id => User.current.id)
              if !array.empty? || !array2.empty?
                @stories << fix_ist
              end
            end
          end
         end
      end
    end

    # temp_stories = RbStory.where(:parent_id => nil)
    # temp_stories.each do |story|
    #   if story.assigned_to == User.current || story.author == User.current
    #     @stories << story
    #   else
    #     array = RbStory.where(:parent_id => story.id, :assigned_to_id => User.current.id)
    #       unless array.empty?
    #         @stories << story
    #       end
    #   end 
    # end
    @story_ids = @stories.map{|s| s.id}

    @stories.sort_by! {|a| a.status.is_closed? ? 1 : 0 }

    @settings = Backlogs.settings

    ## determine status columns to show
    tracker = Tracker.find_by_id(RbTask.tracker)
    statuses = tracker.issue_statuses
    # disable columns by default
    if User.current.admin?
      @statuses = statuses
    else
      enabled = {}
      statuses.each{|s| enabled[s.id] = false}
      # enable all statuses held by current tasks, regardless of whether the current user has access
      @sprints.each do |o_this_sprint|
        RbTask.find(:all, :conditions => ['fixed_version_id = ?', o_this_sprint.id]).each {|task| enabled[task.status_id] = true }
      end

      roles = []
      @projects.each do |prprp|
        roles += User.current.roles_for_project(prprp)
      end
      #@transitions = {}
      statuses.each {|status|

        # enable all statuses the current user can reach from any task status
        [false, true].each {|creator|
          [false, true].each {|assignee|

            allowed = status.new_statuses_allowed_to(roles, tracker, creator, assignee).collect{|s| s.id}
            #@transitions["c#{creator ? 'y' : 'n'}a#{assignee ? 'y' : 'n'}"] = allowed
            allowed.each{|s| enabled[s] = true}
          }
        }
      }
      @statuses = statuses.select{|s| enabled[s.id]}
    end

    #if @stories.size == 0
      @last_updated = nil
    # else
    #   @sprints.each do |fff|
    #     @last_updated = RbTask.find(:first,
    #                     :conditions => ['tracker_id = ? and fixed_version_id = ?', RbTask.tracker, fff.id],
    #                     :order      => "updated_on DESC")
    #   end
    # end

    respond_to do |format|
      format.html { render :layout => "rb" }
    end
  end

   #def tooltip
   #  story = RbStory.find(params[:id])
   # respond_to do |format|
     # format.html { render :partial => "tooltip", :object => story }
   # end
 #end

  # private

  # def load_project
  #   @project = if params[:sprint_id]
  #                load_sprint
  #                @sprint.project
  #              elsif params[:release_id] && !params[:release_id].empty?
  #                load_release
  #                @release.project
  #              elsif params[:release_multiview_id] && !params[:release_multiview_id].empty?
  #                load_release_multiview
  #                @release_multiview.project
  #              elsif params[:project_id]
  #                Project.find(params[:project_id])
  #              else
  #                 Project.last
  #                #raise "Cannot determine project (#{params.inspect})"
  #              end
  # end

  # def check_if_plugin_is_configured
  #   @settings = Backlogs.settings
  #   if @settings[:story_trackers].blank? || @settings[:task_tracker].blank?
  #     respond_to do |format|
  #       format.html { render :file => "backlogs/not_configured" }
  #     end
  #   end
  # end

  def save_filters
     cookies[params[:name]]=params[:status]
    end
end
