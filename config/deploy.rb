require 'rvm/capistrano' # Для работы rvm
require 'bundler/capistrano' # Для работы bundler. При изменении гемов bundler автоматически обновит все гемы на сервере, чтобы они в точности соответствовали гемам разработчика. 
# Имя приложения
set :application, "redmine"
# Вызов функции set создаст переменную application и присвоит ей значение "myproject"

# В проекте мы используем только один сервер, выполняющий роль веб-сервиса.
set :rails_env, "production"
set :domain, "kholdrex@server.sloboda-studio.com"
set :password, ""
set :port,  3333
set :user, "kholdrex"
set :use_sudo, false

set :rvm_ruby_string, '1.9.3@backlog' # Это указание на то, какой Ruby интерпретатор мы будем использовать.

role :web, domain
role :app, domain
role :db,  domain, :primary => true
# Директория приложения на удалённом хостинге
set :app_dir, "/home/#{user}/projects/#{application}" # /home/myuser/myproject/
# Запись "/home/#{user}/#{application}/" аналогична "/home/$user/$application/" в PHP

# Директория, куда будет делаться checkout из репозитория
set :deploy_to, "#{app_dir}" # /home/myuser/myproject/deploy

set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid, "#{deploy_to}/shared/pids/unicorn.pid"

set :scm, :git
set :repository,  "git@bitbucket.org:kholdrex/backlogs.git"
set :branch, "master" 
set :remote, "origin"

set :deploy_via, :copy # Указание на то, что стоит хранить кеш репозитария локально и с каждым деплоем лишь подтягивать произведенные изменения. Очень актуально для больших и тяжелых репозитариев.

before 'deploy:setup', 'rvm:install_rvm', 'rvm:install_ruby' # интеграция rvm с capistrano настолько хороша, что при выполнении cap deploy:setup установит себя и указанный в rvm_ruby_string руби.

# before 'deploy:finalize_update', roles => :app do
#  run "ln -s #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
# end

after 'deploy:update_code', :roles => :app do
 # run "ln -s #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
 #run "rm #{release_path}/config/unicorn.rb"
 #run "rm #{current_path}/config/unicorn.rb"
 #run "ln -s #{deploy_to}/shared/config/unicorn.rb #{current_path}/config/unicorn.rb"
 run "cp #{deploy_to}/shared/config/unicorn.rb #{release_path}/config/unicorn.rb"
end

after 'deploy:create_symlink', :roles => :app do
  run "ln -s #{deploy_to}/shared/files #{release_path}/files"
  run "ln -s #{deploy_to}/shared/plugins/redmine_backlogs #{release_path}/plugins/redmine_backlogs"
end

# Далее идут правила для перезапуска unicorn. Их стоит просто принять на веру - они работают.
# В случае с Rails 3 приложениями стоит заменять bundle exec unicorn_rails на bundle exec unicorn
namespace :deploy do
  task :restart do
    run "cd #{current_path} && if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -USR2 `cat #{unicorn_pid}`; else cd #{deploy_to}/current && bundle exec unicorn_rails -c #{unicorn_conf} -E #{rails_env} -D; fi"
  end
  task :start do
    run "cd #{current_path} && bundle exec unicorn_rails -c #{unicorn_conf} -E #{rails_env} -D"
  end
  task :stop do
    run "cd #{current_path} && if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -QUIT `cat #{unicorn_pid}`; fi"
  end
end