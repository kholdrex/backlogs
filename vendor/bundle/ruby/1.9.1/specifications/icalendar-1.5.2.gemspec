# -*- encoding: utf-8 -*-
# stub: icalendar 1.5.2 ruby lib

Gem::Specification.new do |s|
  s.name = "icalendar"
  s.version = "1.5.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Ryan Ahearn"]
  s.date = "2014-03-22"
  s.description = "    Implements the iCalendar specification (RFC-2445) in Ruby.  This allows\n    for the generation and parsing of .ics files, which are used by a\n    variety of calendaring applications.\n"
  s.email = ["ryan.c.ahearn@gmail.com"]
  s.homepage = "https://github.com/icalendar/icalendar"
  s.rubygems_version = "2.2.2"
  s.summary = "A ruby implementation of the iCalendar specification (RFC-2445)."

  s.installed_by_version = "2.2.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, ["~> 10.0"])
      s.add_development_dependency(%q<bundler>, ["~> 1.3"])
      s.add_development_dependency(%q<tzinfo>, ["~> 0.3"])
      s.add_development_dependency(%q<timecop>, ["~> 0.6.3"])
    else
      s.add_dependency(%q<rake>, ["~> 10.0"])
      s.add_dependency(%q<bundler>, ["~> 1.3"])
      s.add_dependency(%q<tzinfo>, ["~> 0.3"])
      s.add_dependency(%q<timecop>, ["~> 0.6.3"])
    end
  else
    s.add_dependency(%q<rake>, ["~> 10.0"])
    s.add_dependency(%q<bundler>, ["~> 1.3"])
    s.add_dependency(%q<tzinfo>, ["~> 0.3"])
    s.add_dependency(%q<timecop>, ["~> 0.6.3"])
  end
end
